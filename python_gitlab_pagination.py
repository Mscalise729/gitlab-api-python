#!/usr/bin/env python

# Description: Use keyset pagination to search for a group name
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys
from timeit import default_timer as timer

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

SEARCH_GROUP_NAME="everyonecancontribute"

# Use keyset pagination
# https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination
gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN,
    pagination="keyset", order_by="id", per_page=100)

# Iterate over the list, and fire new API calls in case the result set does not match yet
groups = gl.groups.list(iterator=True)

found_page = 0
start = timer()

for group in groups:
    if SEARCH_GROUP_NAME == group.name:
        # print(group) # debug
        found_page = groups.current_page
        break

end = timer()

duration = f'{end-start:.2f}'

if found_page > 0:
    print("Pagination API example for Python with GitLab{desc} - found group {g} on page {p}, duration {d}s".format(
        desc=", the DevSecOps platform", g=SEARCH_GROUP_NAME, p=found_page, d=duration))
else:
    print("Could not find group name '{g}', duration {d}".format(g=SEARCH_GROUP_NAME, d=duration))


