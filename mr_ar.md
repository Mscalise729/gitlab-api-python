# Summary of projects and their merge request approval rules
# Project: Developer Evangelism at GitLab  / use-cases / GitLab API / GitLab API Playground, ID: 42489449


[MR Approval settings](https://gitlab.com/gitlab-de/use-cases/gitlab-api/gitlab-api-playground/-/settings/merge_requests)


## Approval rule: Maintainer approval required , ID: 6684764

```json

{
  "project_id": "42489449",
  "id": 6684764,
  "name": "Maintainer approval required ",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "approvals_required": 1,
  "users": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "groups": [],
  "contains_hidden_groups": false,
  "protected_branches": [],
  "applies_to_all_protected_branches": false
}

```

## Approval rule: Protected branch approval , ID: 6684767

```json

{
  "project_id": "42489449",
  "id": 6684767,
  "name": "Protected branch approval ",
  "rule_type": "regular",
  "eligible_approvers": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "approvals_required": 1,
  "users": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "groups": [],
  "contains_hidden_groups": false,
  "protected_branches": [
    {
      "id": 62297497,
      "name": "main",
      "push_access_levels": [
        {
          "id": 68405664,
          "access_level": 40,
          "access_level_description": "Maintainers",
          "user_id": null,
          "group_id": null
        }
      ],
      "merge_access_levels": [
        {
          "id": 67163394,
          "access_level": 40,
          "access_level_description": "Maintainers",
          "user_id": null,
          "group_id": null
        }
      ],
      "allow_force_push": false,
      "unprotect_access_levels": [],
      "code_owner_approval_required": false
    }
  ],
  "applies_to_all_protected_branches": true
}

```

## Approval rule: Coverage-Check, ID: 6684770

```json

{
  "project_id": "42489449",
  "id": 6684770,
  "name": "Coverage-Check",
  "rule_type": "report_approver",
  "eligible_approvers": [
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    },
    {
      "id": 260236,
      "username": "abuango",
      "name": "Abubakar Siddiq Ango",
      "state": "active",
      "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/260236/avatar.png",
      "web_url": "https://gitlab.com/abuango"
    }
  ],
  "approvals_required": 0,
  "users": [
    {
      "id": 260236,
      "username": "abuango",
      "name": "Abubakar Siddiq Ango",
      "state": "active",
      "avatar_url": "https://gitlab.com/uploads/-/system/user/avatar/260236/avatar.png",
      "web_url": "https://gitlab.com/abuango"
    },
    {
      "id": 5534214,
      "username": "dnsmichi",
      "name": "Michael Friedrich",
      "state": "active",
      "avatar_url": "https://secure.gravatar.com/avatar/383a41b48a683b47432b15e8e86fd285?s=80&d=identicon",
      "web_url": "https://gitlab.com/dnsmichi"
    }
  ],
  "groups": [],
  "contains_hidden_groups": false,
  "protected_branches": [],
  "applies_to_all_protected_branches": false
}

```

