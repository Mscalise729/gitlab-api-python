#!/usr/bin/env python

# Description: Fetch audit events (all, group, project)
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys
import json

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires maintainer permissions
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
GROUP_ID = os.environ.get('GL_GROUP_ID') #optional

#################
# Main

if __name__ == "__main__":
    if not GITLAB_TOKEN:
        print("🤔 Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

    print("# Starting...", end="", flush=True)

    # Collect all projects, or prefer projects from a group id, or a project id
    projects = []

    # Direct project ID
    if PROJECT_ID:
        projects.append(gl.projects.get(PROJECT_ID))

    # Groups and projects inside
    elif GROUP_ID:
        group = gl.groups.get(GROUP_ID)

        for project in group.projects.list(include_subgroups=True, all=True):
            print(".", end="", flush=True) # Get some feedback that it is still looping
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            manageable_project = gl.projects.get(project.id , lazy=True)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        projects = gl.projects.list(get_all=True)

    print(f"\n# Found {len(projects)} projects. Analysing...", end="", flush=True) # new line

    events = {}

    # Loop over projects, fetch audit events 
    for project in projects:
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/ci_lint.html
            project_obj = gl.projects.get(project.id)
            project_name = project_obj.name
            project_web_url =project_obj.web_url

            for event in project_obj.audit_events.list():
                event_obj = project_obj.audit_events.get(event.id)
                event_entity_type = event_obj.entity_type
                event_entity_id = event_obj.entity_id
                event_details = event_obj.details
                event_created_at = event_obj.created_at

                print(event_entity_id, event_entity_type, event_created_at, end="\n\n", flush=True)
                print(json.dumps(event_details, indent=4), end="", flush=True)


    print("\nDone")
