#!/usr/bin/env python

# Description: Fetch projects (all, group, project) and print and disable latest artifacts setting updating them to false
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Ricardo Amarilla <ramarilla@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires maintainer+ permissions
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
GROUP_ID = os.environ.get('GL_GROUP_ID') #optional

#################
# Main

if __name__ == "__main__":
    if not GITLAB_TOKEN:
        print("🤔 Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

    # Collect all projects, or prefer projects from a group id, or a project id
    projects = []

    # Direct project ID
    if PROJECT_ID:
        projects.append(gl.projects.get(PROJECT_ID))

    # Groups and projects inside
    elif GROUP_ID:
        group = gl.groups.get(GROUP_ID)

        for project in group.projects.list(include_subgroups=True, all=True):
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            manageable_project = gl.projects.get(project.id)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        projects = gl.projects.list(get_all=True)

    print("# Before - Summary of projects and their latest artifacts configuration status ")

    print("|Project|latest artifacts configuration status|\n|-|-|")
    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html
    for project in projects:
           print(f"|{project.name}|{project.keep_latest_artifact}|")

    # Change the keep latest articact to false if is true for each project

    for project in projects:
        if project.keep_latest_artifact:
            project.keep_latest_artifact = False
            project.save()

    print("# After - Summary of projects and their latest artifacts configuration status ")
    print("|Project|latest artifacts configuration status|\n|-|-|")
    for project in projects:
           print(f"|{project.name}|{project.keep_latest_artifact}|")    

sys.exit(0)
