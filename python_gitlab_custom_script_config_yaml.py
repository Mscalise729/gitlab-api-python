#!/usr/bin/env python

# Description: Show how a yaml file can be parsed
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import os
import yaml

CONFIG_FILE = os.environ.get('GL_CONFIG_FILE', "python_gitlab_custom_yaml_config.yml")

# Read config
with open(CONFIG_FILE, mode="rt", encoding="utf-8") as file:
    config = yaml.safe_load(file)
    #print(config) #debug

tasks = []
if "tasks" in config:
    tasks = config['tasks']

# Process the tasks
for task in tasks:
    print("Task name: '{n}' Issue URL to update: {id}".format(n=task['name'], id=task['url']))
    # print(task) #debug
