#!/usr/bin/env python

# Description: Show how projects in groups can be managed
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-de/use-cases/
GROUP_ID = os.environ.get('GL_GROUP_ID', 16058698)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

# Main
group = gl.groups.get(GROUP_ID)

# Collect all projects in group and subgroups
projects = group.projects.list(include_subgroups=True, all=True)

for project in projects:
    # Try running a method on a weak object
    try:
       print("🤔 Project: {pn} 💡 Branches: {b}\n".format(
        pn=project.name,
        b=", ".join([x.name for x in project.branches.list()])))
    except Exception as e:
        print("Got exception: {e} \n ===================================== \n".format(e=e))

    # Retrieve a full manageable project object
    # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
    manageable_project = gl.projects.get(project.id)

    # Print a method available on a manageable object
    print("🤔 Project: {pn} 💡 Branches: {b}\n".format(
        pn=manageable_project.name,
        b=", ".join([x.name for x in manageable_project.branches.list()])))

