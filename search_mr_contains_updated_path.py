#!/usr/bin/env python

# Description: Fetch MRs from a given project ID and check for modified file paths; helps analyse potential merge conflicts after large refactoring MRs.
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

import gitlab
import os
import sys

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
# https://gitlab.com/gitlab-com/www-gitlab-com
PROJECT_ID = os.environ.get('GL_PROJECT_ID', 7764)
GITLAB_TOKEN = os.environ.get('GL_TOKEN')

if not GITLAB_TOKEN:
    print("Please set the GL_TOKEN env variable.")
    sys.exit(1)

# TOKEN requires read only permissions on MRs.
gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

PATH_PATTERNS = [
    'sites/handbook/source/handbook/marketing',
]

IGNORE_MRS = [
    115464, # marketing re-org MR part 1
    115480, # marketing re-org MR part 2
]

# Main
project = gl.projects.get(PROJECT_ID, lazy=False, pagination="keyset", order_by="updated_at", per_page=100)

# Only list opened MRs
# https://python-gitlab.readthedocs.io/en/stable/gl_objects/merge_requests.html#project-merge-requests
mrs = project.mergerequests.list(state='opened', iterator=True)

seen_mr = {}

for mr in mrs:
    #print(mr.attributes)

    # https://docs.gitlab.com/ee/api/merge_requests.html#list-merge-request-diffs
    real_mr = project.mergerequests.get(mr.get_id())
    real_mr_id = real_mr.attributes['iid']
    real_mr_url = real_mr.attributes['web_url']

    # skip MRs that are the real changes and cause false positives
    if real_mr_id in IGNORE_MRS:
        print("DEBUG: Ignoring MR {id}".format(id=real_mr_id))
        continue

    # Only test MRs that do not have merge conflicts, and have green CI/CD pipelines
    mr_status = real_mr.attributes['detailed_merge_status']

    #if mr_status not in ['mergable']:
    #    print("Skipping not mergeable MRs")
    #    continue

    for diff in real_mr.diffs.list(iterator=True):
        real_diff = real_mr.diffs.get(diff.id)

        for d in real_diff.attributes['diffs']:
            for p in PATH_PATTERNS:
                if p in d['old_path']:
                    print("MATCH: {p} in MR {mr_id}, status '{s}', title '{t}' - URL: {mr_url}".format(
                        p=p,
                        mr_id=real_mr_id,
                        s=mr_status,
                        t=real_mr.attributes['title'],
                        mr_url=real_mr_url))

                    if not real_mr_id in seen_mr:
                        seen_mr[real_mr_id] = real_mr

# TODO: Do something with all matches

print("\n# MRs to update\n")

for id, real_mr in seen_mr.items():
    print("- [ ] !{mr_id} - {mr_url}+ Status: {s}, Title: {t}".format(
        mr_id=id,
        mr_url=real_mr.attributes['web_url'],
        s=real_mr.attributes['detailed_merge_status'],
        t=real_mr.attributes['title']))

    # TODO: Add an automated comment that links the MR that introduces changes?




