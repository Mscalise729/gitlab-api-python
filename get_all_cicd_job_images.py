#!/usr/bin/env python

# Description: Fetch projects (all, group, project), get the merged linted CI/CD configuration, and print the image string
# Requirements: python-gitlab Python libraries. GitLab API read access, and maintainer access to all configured groups/projects.
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2023-present GitLab B.V.

from typing import Union
import gitlab
import os
import sys
import yaml
import re

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN') # token requires developer permissions
PROJECT_ID = os.environ.get('GL_PROJECT_ID') #optional
# https://gitlab.com/gitlab-de/use-cases/docker
GROUP_ID = os.environ.get('GL_GROUP_ID', "XXXX") #optional

#################
# Main

if __name__ == "__main__":
    if not GITLAB_TOKEN:
        print("🤔 Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)

    # Create regex to exclude trusted images
    # Trusted content is not affected https://hub.docker.com/search?q=&image_filter=official%2Cstore%2Copen_source
    re_variables='.*(ANALYZER|\$CI_IMAGE|\${CI_IMAGE}|\$GOLANG_IMAGE|\${TANKA_IMAGE}|\$WOODHOUSE_IMAGE).*' # Exclude generic variables
    re_trusted_registries='^(\${CI_REGISTRY}|\${CI_REGISTRY_IMAGE|\$CI_REGISTRY_IMAGE|public.ecr.aws|ghcr.io|gcr.io|registry.gitlab.com).*'
    re_official_images='^(ruby|docker|golang|ubuntu|google|alpine|node|python):?.*'
    re_verified_publisher_namespaces='^(hashicorp|grafana|amazon|docker)\/.*'
    re_sponsored_oss='^(curlimages)\/.*'
    exclude_images_pattern = f'{re_variables}|{re_trusted_registries}|{re_official_images}|{re_verified_publisher_namespaces}|{re_sponsored_oss}'

    print("# Starting...", end="", flush=True)

    # Collect all projects, or prefer projects from a group id, or a project id
    projects = []

    # Direct project ID
    if PROJECT_ID:
        projects.append(gl.projects.get(PROJECT_ID))

    # Groups and projects inside
    elif GROUP_ID:
        group = gl.groups.get(GROUP_ID)

        for project in group.projects.list(include_subgroups=True, all=True):
            print(".", end="", flush=True) # Get some feedback that it is still looping
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            manageable_project = gl.projects.get(project.id , lazy=True)
            projects.append(manageable_project)

    # All projects on the instance (may take a while to process)
    else:
        projects = gl.projects.list(get_all=True)

    print(f"\n# Found {len(projects)} projects. Analysing...", end="", flush=True) # new line

    ci_images = {}
    dockerfile_images = {}

    # Loop over projects, fetch .gitlab-ci.yml, run the linter to get the full translated config, and extract the `image:` setting
    for project in projects:
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/ci_lint.html
            project_obj = gl.projects.get(project.id)
            project_name =project_obj.name
            project_web_url =project_obj.web_url

            ####################################
            # Search for images on ci_pipelines
            ####################################

            try:
                lint_result = project.ci_lint.get()

                if lint_result.merged_yaml is None:
                    continue

                ci_pipeline = yaml.safe_load(lint_result.merged_yaml)

                for job_name in ci_pipeline:
                    job_obj = ci_pipeline[job_name]
                    if isinstance(job_obj, dict) and 'image' in job_obj:
                        print(".", end="", flush=True) # Get some feedback that it is still looping
                        image_obj = job_obj['image']
                        image: Union[any, str] =  image_obj if isinstance(image_obj, str) else image_obj["name"]
                        result = re.match(exclude_images_pattern, image)
                        if result:
                            continue
                        ci_images[project_web_url] = { 'project_name': project_name, 'job_name': job_name, 'image': image}

            except Exception:
            #except Exception as e:
                # print(f"Exception searching images on ci_pipelines: {e}")
                continue # Silent continue

    print("\nDone")

    if len(ci_images) > 0:
        print("|Project|Job|Image|\n|-|-|-|") # Start makdown friendly table
        for project_web_url, details in ci_images.items():
            print(f'| [{ details["project_name"] }]({project_web_url}) | { details["job_name"] } | { details["image"]  } |')
